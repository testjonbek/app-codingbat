package uz.pdp.codingbat.entity.role;

public enum Role {
    ADMIN, USER, MODERATOR
}
